# React Native Training

This is a sample app used for RN training. It contains basic setup with some basic libraries and some
sample functionalities.

## Running the app

`yarn` To install packages

`pod install` We have firebase installed as a pod, so you need to run pod install in ios-folder

`yarn ios` or `yarn android`

## Package.json breakdown

For React Native app you normally need all the RN core libraries, navigator library, state management library, some library to support asynchronous actions + others depending of your requirements.

**Core**

* [react](https://reactjs.org/)
* [react-native](https://facebook.github.io/react-native/)

**Navigation**

* [react-native-navigation](https://github.com/wix/react-native-navigation/tree/v2) For native style navigation

**State Management with redux**

* [redux](https://redux.js.org/basics/usagewithreact) Redux state management
* [react-redux](https://github.com/reduxjs/react-redux) React integration
* [redux-persist](https://github.com/rt2zz/redux-persist) For persisting redux states
* [redux-logic](https://github.com/jeffbski/redux-logic) For adding asynchronous logic
* [redux-devtools-extension](https://github.com/zalmoxisus/redux-devtools-extension) To support [React Native Debugger](https://github.com/jhen0409/react-native-debugger)

**For supporting different deploy environments**

* [react-native-config](https://github.com/luggit/react-native-config) To support dev/production envinronments

**For supporting push messages**

* [react-native-firebase](https://github.com/invertase/react-native-firebase) To support push messages

**Utils**

* [ramda](https://ramdajs.com/) Just some utils


## Things to learn

* When installing native libraries it's common to have some problems with automatic linking, so it's good to learn how it's done manually. [Linking Libraries](https://facebook.github.io/react-native/docs/linking-libraries-ios)


## Considerations

For the production use you may consider adding:

1. Localizations (react-native-intl, react-i18next)
2. Crash reporting (Firebase, AppCenter, Sentry)
3. Analytics (GA, Firebase, AppCenter) & integrate with Saga or Redux Logic
