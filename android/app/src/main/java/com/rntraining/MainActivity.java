package com.rntraining;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.widget.LinearLayout;
import android.graphics.Color;
import android.view.Gravity;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import com.reactnativenavigation.NavigationActivity;

public class MainActivity extends NavigationActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSplashLayout();
    }

    private void setSplashLayout() {
        LinearLayout view = new LinearLayout(this);
        ImageView imageView = new ImageView(this);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(256, 256);
        imageView.setLayoutParams(layoutParams);

        view.setBackgroundColor(Color.parseColor("#9e9854"));
        view.setGravity(Gravity.CENTER);
        imageView.setImageDrawable(ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.logo));

        view.addView(imageView);

        setContentView(view);
    }
}
