const blacklist = require('metro-config/src/defaults/blacklist');

module.exports = {
  watchFolders: [],
  resolver: {
    blacklistRE: blacklist([/ios\/Pods\/.*/])
  },
  transformer: {
    babelTransformerPath: require.resolve('react-native-typescript-transformer'),
  },
};
