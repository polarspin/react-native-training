import { Navigation } from 'react-native-navigation';
import { Store } from 'redux';
import { NativeConfig } from 'react-native-config';
import { logger } from './logger';
import { init, start } from './store/app';
import { AppState, BackHandler } from 'react-native';
import { ScreenProvider } from './Providers';

export class App {
  private store: Store;
  private config: NativeConfig;

  constructor(store: Store, config: NativeConfig) {
    this.store = store;
    this.config = config;
  }

  registerScreen(name: string, component: any) {
    Navigation.registerComponentWithRedux(name, () => component, ScreenProvider, this.store);
  }

  start = async () => {
    logger.log('Starting App with config', this.config);

    // Add some native event handlers
    BackHandler.addEventListener('hardwareBackPress', () => {});

    this.store.dispatch(init());
    await Navigation.setRoot({
      root: {
        sideMenu: {
          id: 'root',
          right: {
            component: { name: 'SideMenu' },
          },
          center: {
            stack: {
              children: [
                {
                  component: {
                    name: 'MainScreen',
                  },
                },
              ],
            },
          },
        },
      },
    });
    this.store.dispatch(start());
  }
}
