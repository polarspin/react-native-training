import React, { ReactNode } from 'react';
import { Provider } from 'react-redux';
import { store } from './store';

export const ScreenProvider = ({ children }: { children: ReactNode }) => (
  <Provider store={store}>
    {children}
  </Provider>
);
