/* tslint:disable:no-console */
import { logger } from '../logger';

describe('Logger', () => {
  it('should check that logger.log forward call to console.log', () => {
    spyOn(console, 'log');
    logger.log('abc', { data: 1 }, 'qwerty');

    expect(console.log).toHaveBeenCalledWith('abc', { data: 1 }, 'qwerty');
  });

  it('should check that logger.warn prints error in console', () => {
    spyOn(console, 'warn');
    logger.warn('abc', { data: 1 }, 1);

    expect(console.warn).toHaveBeenCalledWith('abc', { data: 1 }, 1);
  });
});
