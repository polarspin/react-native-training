import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { Text } from './Text';
import { theme } from '../theme';

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: theme.colors.backgroundWhite,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: theme.colors.textBlack,
    fontSize: 16,
  },
});

interface Props {
  text: string;
  onPress: () => void;
}

export const Button = ({ onPress, text }: Props) => (
  <TouchableOpacity onPress={onPress}>
    <View style={styles.container}>
      <Text style={styles.text}>{text}</Text>
    </View>
  </TouchableOpacity>
);
