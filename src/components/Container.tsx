import React, { ReactNode } from 'react';
import { View, StyleProp, ViewStyle, StyleSheet } from 'react-native';
import { theme } from '../theme';

const styles = StyleSheet.create({
  default: {
    flex: 1,
  },
  header: {
    flex: 0,
    width: '100%',
    height: 44,
    backgroundColor: theme.colors.backgroundBlack,
    paddingHorizontal: 20,
    justifyContent: 'center',
  },
  footer: {
    flex: 0,
  },
});

interface Props {
  type?: 'default' | 'footer' | 'header';
  children: ReactNode;
  style?: StyleProp<ViewStyle>;
}
export const Container = ({ type = 'default', style, children }: Props) => (
  <View style={[styles[type], style]}>{children}</View>
);
