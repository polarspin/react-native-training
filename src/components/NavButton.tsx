import React from 'react';
import { StyleSheet, StyleProp, ViewStyle, View, TouchableOpacity } from 'react-native';
import { theme } from '../theme';

const styles = StyleSheet.create({
  container: {
    flex: 0,
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: theme.colors.backgroundWhite,
  },
});

interface Props {
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
}

export const NavButton = ({ style, onPress }: Props) => (
  <TouchableOpacity onPress={onPress}>
    <View style={[styles.container, style]} />
  </TouchableOpacity>
);
