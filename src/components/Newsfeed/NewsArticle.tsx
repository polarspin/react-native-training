import React from 'react';
import { Text } from '../Text';
import { Article } from '../../services/newsService';
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { theme } from '../../theme';

const styles = StyleSheet.create({
  container: {
    flex: 0,
    width: '100%',
    backgroundColor: theme.colors.backgroundWhite,
    padding: 10,
    marginBottom: 10,
  },
  title: {
    color: theme.colors.textBlack,
    fontWeight: '600',
    fontSize: 16,
  },
  text: {
    flex: 1,
    color: theme.colors.textBlack,
    fontSize: 12,
  },
  content: {
    flexDirection: 'row',
  },
  image: {
    width: 100,
    height: 100,
  },
});

interface Props {
  article: Article;
  onClick: () => void;
}

export const NewsArticle = ({ article, onClick }: Props) => (
  <TouchableOpacity onPress={onClick}>
    <View style={styles.container}>
      <Text style={styles.title}>{article.title}</Text>
      <View style={styles.content}>
        <Text style={styles.text}>{article.description}</Text>
        <Image
          style={styles.image as any}
          source={{ uri: article.urlToImage }}
          resizeMode="cover"
        />
      </View>
    </View>
  </TouchableOpacity>
);
