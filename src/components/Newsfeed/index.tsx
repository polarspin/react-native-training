import React, { PureComponent } from 'react';
import { Text } from '../Text';
import { Article } from '../../services/newsService';
import { FlatList } from 'react-native';
import { NewsArticle } from './NewsArticle';

interface Props {
  loading?: boolean;
  articles: Article[];
  onRequestRefresh?: () => void;
  onRequestMore?: () => void;
  onArticleClick: (article: Article) => void;
}
export class Newsfeed extends PureComponent<Props> {

  private keyExtractor = (item: Article) => item.url;
  private renderItem = ({ item }: { item: Article }) => {
    const { onArticleClick } = this.props;
    return <NewsArticle article={item} onClick={() => onArticleClick(item)} />;
  }
  render() {
    const { articles, onRequestRefresh, onRequestMore, loading = false } = this.props;
    return (
      <FlatList
        data={articles}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
        onRefresh={onRequestRefresh}
        refreshing={loading}
        onEndReached={onRequestMore}
      />
    );
  }
}
