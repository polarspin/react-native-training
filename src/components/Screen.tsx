import React, { ReactNode } from 'react';
import { StyleSheet, StyleProp, ViewStyle, SafeAreaView, View } from 'react-native';
import { theme } from '../theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.backgroundBlack,
  },
  content: {
    flex: 1,
    backgroundColor: theme.colors.backgroundGrey,
  },
});

interface Props {
  style?: StyleProp<ViewStyle>;
  children?: ReactNode;
}

export const Screen = ({ style, children }: Props) => (
  <SafeAreaView style={styles.container}>
    <View style={[styles.content, style]}>
      {children}
    </View>
  </SafeAreaView>
);
