import React from 'react';
import { Text as RNText, TextProps, StyleSheet } from 'react-native';
import { theme } from '../theme';

const styles = StyleSheet.create({
  text: {
    color: theme.colors.textWhite,
    fontSize: 16,
  },
});

interface Props extends TextProps {
  children: any;
}

export const Text = ({ children, style, ...rest}: Props) => (
  <RNText style={[styles.text, style]} {...rest}>{children}</RNText>
);
