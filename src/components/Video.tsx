import React from 'react';
import RNVideo from 'react-native-video';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

interface Props {
  uri: string;
}

export const Video = ({ uri }: Props) => (
  <RNVideo
    style={styles.container}
    controls={true}
    repeat={true}
    source={{ uri }}
  />
);
