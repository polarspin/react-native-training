import Config from 'react-native-config';
import { Navigation } from 'react-native-navigation';
import { App } from './App';
import { store, rehydrated } from './store';
import { defaultNavOptions } from './navOptions';

import SideMenu from './screens/SideMenu';
import NavMenuButton from './screens/NavMenuButton';

// App screens
import MainScreen from './screens/MainScreen';
import WebViewScreen from './screens/WebViewScreen';
import VideoScreen from './screens/VideoScreen';

const app = new App(store, Config);

app.registerScreen('SideMenu', SideMenu);
app.registerScreen('NavMenuButton', NavMenuButton);
app.registerScreen('MainScreen', MainScreen);
app.registerScreen('WebViewScreen', WebViewScreen);
app.registerScreen('VideoScreen', VideoScreen);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setDefaultOptions(defaultNavOptions as any);
  rehydrated.then(() => {
    app.start();
  });
});
