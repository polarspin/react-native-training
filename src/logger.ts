
export interface Logger {
  log(msg: string, ...args: any[]): void;
  warn(msg: string, ...args: any[]): void;
  error(msg: string, ...args: any[]): void;
}

export const logger: Logger = {
  log: (msg: string, ...args: any[]): void => {
    if (__DEV__) {
      console.log(msg, ...args); // tslint:disable-line no-console
    }
  },
  warn: (msg: string, ...args: any[]): void => {
    if (__DEV__) {
      console.warn(msg, ...args); // tslint:disable-line no-console
    }
  },
  error: (msg: string, ...args: any[]): void => {
    if (__DEV__) {
      console.error(msg, ...args); // tslint:disable-line no-console
    }
    try {
      // TODO: Send error data to Sentry/Crashlytics or somewhere
    } catch (e) {
      if (__DEV__) {
        console.error('Error reporting failed', e); // tslint:disable-line no-console
      }
    }
  },
};
