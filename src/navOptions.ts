import { theme } from './theme';

const defaultLayout = {
  layout: {
    backgroundColor: 'white',
    orientation: ['portrait'],
  },
};
const defaultStatusBar = {
  statusBar: {
    visible: true,
    style: 'light',
    backgroundColor: theme.colors.backgroundBlack,
    drawBehind: false,
  },
};
const topBarCommon = {
  visible: true,
  noBorder: true,
  borderColor: '#000',
  borderHeight: 0,
  elevation: 0,
  title: {
    text: 'Sample App',
    color: theme.colors.textWhite,
  },
  backButton: {
    showTitle: false,
    color: theme.colors.textWhite,
  },
};

export const defaultNavOptions = {
  ...defaultLayout,
  ...defaultStatusBar,
  topBar: {
    ...topBarCommon,
    background: {
      color: theme.colors.backgroundBlack,
    },
    rightButtons: [
      { id: 'menuButton', component: { id: 'navMenuBtn', name: 'NavMenuButton' }},
    ],
  },
};
