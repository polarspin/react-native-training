import React from 'react';
import { Screen } from '../components/Screen';
import { Article } from '../services/newsService';
import { Newsfeed } from '../components/Newsfeed';
import { RootState } from '../store/types';
import { getArticles, isLoading, loadArticles, loadMoreArticles, openArticle } from '../store/articles';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

interface StateProps {
  loading: boolean;
  articles: Article[];
}
interface DispatchProps {
  onRefresh: () => void;
  onLoadMore: () => void;
  onArticleClick: (article: Article) => void;
}
interface OwnProps {
}

type Props = StateProps & DispatchProps & OwnProps;

export const MainScreen = ({ articles, loading, onRefresh, onLoadMore, onArticleClick }: Props) => (
  <Screen>
    <Newsfeed
      articles={articles}
      loading={loading}
      onRequestRefresh={onRefresh}
      onRequestMore={onLoadMore}
      onArticleClick={onArticleClick}
    />
  </Screen>
);

const mapStateToProps = (state: RootState): StateProps => ({
  loading: isLoading(state),
  articles: getArticles(state),
});
const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  onRefresh: () => dispatch(loadArticles()),
  onLoadMore: () => dispatch(loadMoreArticles()),
  onArticleClick: (article: Article) => dispatch(openArticle(article)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
