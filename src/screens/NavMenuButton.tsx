import React from 'react';
import { RootState } from '../store/types';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { NavButton } from '../components/NavButton';
import { openSidemenu } from '../store/nav';

interface StateProps {
}
interface DispatchProps {
  onPress: () => void;
}
interface OwnProps {
}

type Props = StateProps & DispatchProps & OwnProps;

export const NavMenuButton = ({ onPress }: Props) => (
  <NavButton onPress={onPress} />
);

const mapStateToProps = (state: RootState): StateProps => ({
});
const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  onPress: () => dispatch(openSidemenu()),
});

export default connect(mapStateToProps, mapDispatchToProps)(NavMenuButton);
