import React from 'react';
import { Screen } from '../components/Screen';
import { RootState } from '../store/types';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Text } from '../components/Text';
import { Container } from '../components/Container';
import { showModal } from '../store/nav';
import { Button } from 'react-native';

interface StateProps {
}
interface DispatchProps {
  onShowVideo: () => void;
}
interface OwnProps {
}

type Props = StateProps & DispatchProps & OwnProps;

export const SideMenu = ({ onShowVideo }: Props) => (
  <Screen>
    <Container type="header">
      <Text>Menu</Text>
    </Container>
    <Container>
      <Button onPress={onShowVideo} title="Show video" />
    </Container>
  </Screen>
);

const mapStateToProps = (state: RootState): StateProps => ({
});
const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  onShowVideo: () => dispatch(showModal('VideoScreen')),
});

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);
