import React from 'react';
import { Screen } from '../components/Screen';
import { Container } from '../components/Container';
import { RootState } from '../store/types';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Button } from '../components/Button';
import { dismissModal } from '../store/nav';
import { Video } from '../components/Video';

interface StateProps {
}
interface DispatchProps {
  onClose: () => void;
}
interface OwnProps {
  url: string;
}

type Props = StateProps & DispatchProps & OwnProps;

export const VideoScreen = ({ url, onClose }: Props) => (
  <Screen>
    <Container>
      <Video uri="https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4" />
    </Container>
    <Container type="footer">
      <Button text="Close" onPress={onClose} />
    </Container>
  </Screen>
);

const mapStateToProps = (state: RootState): StateProps => ({
});
const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  onClose: () => dispatch(dismissModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(VideoScreen);
