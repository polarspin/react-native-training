import React from 'react';
import { Screen } from '../components/Screen';
import { Container } from '../components/Container';
import { RootState } from '../store/types';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { WebView } from 'react-native';
import { LoadingIndicator } from '../components/LoadingIndicator';
import { Button } from '../components/Button';
import { dismissModal } from '../store/nav';

interface StateProps {
}
interface DispatchProps {
  onClose: () => void;
}
interface OwnProps {
  url: string;
}

type Props = StateProps & DispatchProps & OwnProps;

export const WebViewScreen = ({ url, onClose }: Props) => (
  <Screen>
    <Container>
      <WebView
        source={{ uri: url }}
        startInLoadingState={true}
        renderLoading={LoadingIndicator}
      />
    </Container>
    <Container type="footer">
      <Button text="Close" onPress={onClose} />
    </Container>
  </Screen>
);

const mapStateToProps = (state: RootState): StateProps => ({
});
const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  onClose: () => dispatch(dismissModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(WebViewScreen);
