
export interface Article {
  source: {
    id: string;
    name: string,
  };
  author: string;
  title: string;
  description: string;
  url: string;
  urlToImage: string;
  publishedAt: string;
  content: string;
}
export interface NewsResponse {
  totalResults: number;
  articles: Article[];
}
export interface NewsError {
  code: string;
  message: string;
}

export class NewsService {
  private baseUrl: string;
  private apiKey: string;

  constructor(baseUrl: string, apiKey: string) {
    this.baseUrl = baseUrl;
    this.apiKey = apiKey;
  }

  getArticles = async (pageSize: number, page: number, domain: string): Promise<NewsResponse> => {
    const url = `${this.baseUrl}/everything?apiKey=${this.apiKey}&pageSize=${pageSize}&page=${page}&domains=${domain}`;
    const res = await fetch(url);
    const json = await res.json();

    return json.status === 'ok'
      ? Promise.resolve(json as NewsResponse)
      : Promise.reject(json as NewsError);
  }
}
