import { createAction } from '../actionUtils';
import { GeoPosition } from './types';

export const APP_INIT = 'app/INIT';
export const APP_START = 'app/START';
export const APP_TICK = 'app/TICK';

export const init = () => createAction(APP_INIT);
export const start = () => createAction(APP_START);
export const tick = () => createAction(APP_TICK);
