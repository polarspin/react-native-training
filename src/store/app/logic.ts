import { createLogic } from 'redux-logic';
import { LogicDeps } from '../logic';
import {
  APP_START,
  tick,
} from './actions';

const TICK_INTERVAL = 5000;

const appTickLogic = createLogic({
  type: APP_START,
  warnTimeout: 0,
  process: (_, dispatch, done) => {
    setInterval(() => dispatch(tick()), TICK_INTERVAL);
  },
});

export default [
  appTickLogic,
];
