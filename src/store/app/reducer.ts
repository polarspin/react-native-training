import { AppState, AppAction } from './types';
import {
  APP_TICK,
} from './actions';

const initialState: AppState = {
  currentTime: new Date(),
};

export default (state: AppState = initialState, action: AppAction): AppState => {
  switch (action.type) {
    case APP_TICK:
      return {
        ...state,
        currentTime: new Date(),
      };
    default:
      return state;
  }
};
