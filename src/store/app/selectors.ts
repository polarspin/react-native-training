import { RootState } from '../types';

export const getCurrentTime = (state: RootState) => state.app.currentTime;
