import { AppStateStatus } from 'react-native';

import {
  tick,
  init,
  start,
} from './actions';

export interface GeoPosition {
  timestamp: number;
}

export interface AppState {
  readonly currentTime: Date;
  readonly appState?: AppStateStatus;
}

export type TickAction = ReturnType<typeof tick>;
export type InitAction = ReturnType<typeof init>;
export type StartAction = ReturnType<typeof start>;

export type AppAction = (
  TickAction |
  InitAction |
  StartAction
);
