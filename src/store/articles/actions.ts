import { createAction } from '../actionUtils';
import { Article, NewsError } from '../../services/newsService';

export const LOAD_ARTICLES = 'articles/LOAD_ARTICLES';
export const LOAD_MORE_ARTICLES = 'articles/LOAD_MORE_ARTICLES';
export const LOAD_ARTICLES_SUCCESS = 'articles/LOAD_ARTICLES_SUCCESS';
export const LOAD_ARTICLES_FAIL = 'articles/LOAD_ARTICLES_FAIL';
export const OPEN_ARTICLE = 'articles/OPEN_ARTICLE';

export const loadArticles = () => createAction(LOAD_ARTICLES);
export const loadMoreArticles = () => createAction(LOAD_MORE_ARTICLES);
export const loadArticlesSuccess = (articles: Article[], page: number, append: boolean) => createAction(LOAD_ARTICLES_SUCCESS, { articles, append, page });
export const loadArticlesFail = (error: NewsError) => createAction(LOAD_ARTICLES_FAIL, { error });
export const openArticle = (article: Article) => createAction(OPEN_ARTICLE, { article });
