import { createLogic } from 'redux-logic';
import { LogicDeps } from '../logic';
import {
  LOAD_ARTICLES, loadArticles, loadArticlesFail, loadArticlesSuccess, LOAD_MORE_ARTICLES, OPEN_ARTICLE,
} from './actions';
import { APP_START } from '../app';
import { getLastLoadedPage } from './selectors';
import { OpenArticleAction } from './types';
import { openURL } from '../nav';

const loadOnStartLogic = createLogic({
  type: APP_START,
  process: async (_, dispatch, done) => {
    dispatch(loadArticles());
    done();
  },
});

const loadArticlesLogic = createLogic({
  type: LOAD_ARTICLES,
  process: async ({ news }: LogicDeps, dispatch, done) => {
    try {
      const result = await news.getArticles(50, 1, 'engadget.com');

      dispatch(loadArticlesSuccess(result.articles, 1, false));
    } catch (e) {
      dispatch(loadArticlesFail(e));
    } finally {
      done();
    }
  },
});

const loadMoreArticlesLogic = createLogic({
  type: LOAD_MORE_ARTICLES,
  process: async ({ getState, news }: LogicDeps, dispatch, done) => {
    try {
      const state = getState();
      const lastPage = getLastLoadedPage(state);
      const nextPage = lastPage + 1;
      const result = await news.getArticles(50, nextPage, 'engadget.com');

      dispatch(loadArticlesSuccess(result.articles, nextPage, true));
    } catch (e) {
      dispatch(loadArticlesFail(e));
    } finally {
      done();
    }
  },
});

const openArticleLogic = createLogic({
  type: OPEN_ARTICLE,
  process: async ({ logger, action }: LogicDeps<OpenArticleAction>, dispatch, done) => {
    try {
      dispatch(openURL(action.payload.article.url));
    } catch (e) {
      logger.error('Open article failed', e);
    } finally {
      done();
    }
  },
});

export default [
  loadOnStartLogic,
  loadArticlesLogic,
  loadMoreArticlesLogic,
  openArticleLogic,
];
