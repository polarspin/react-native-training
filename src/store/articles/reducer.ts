import { ArticlesState, ArticlesAction } from './types';
import {
  LOAD_ARTICLES,
  LOAD_ARTICLES_SUCCESS,
  LOAD_ARTICLES_FAIL,
  LOAD_MORE_ARTICLES,
} from './actions';

const initialState: ArticlesState = {
  loading: false,
  loadingMore: false,
  lastPageLoaded: 0,
  articles: [],
};

export default (state: ArticlesState = initialState, action: ArticlesAction): ArticlesState => {
  switch (action.type) {
    case LOAD_ARTICLES:
      return {
        ...state,
        loading: true,
      };
    case LOAD_MORE_ARTICLES:
      return {
        ...state,
        loadingMore: true,
      };
    case LOAD_ARTICLES_SUCCESS:
      return {
        ...state,
        loading: false,
        loadingMore: false,
        lastPageLoaded: action.payload.page,
        articles:
          action.payload.append
            ? state.articles.concat(action.payload.articles)
            : action.payload.articles,
        error: undefined,
      };
    case LOAD_ARTICLES_FAIL:
      return {
        ...state,
        loading: false,
        loadingMore: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
};
