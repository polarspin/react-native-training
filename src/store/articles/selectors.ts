import { RootState } from '../types';

export const isLoading = (state: RootState) => state.articles.loading;
export const getArticles = (state: RootState) => state.articles.articles;
export const getLastLoadedPage = (state: RootState) => state.articles.lastPageLoaded;
