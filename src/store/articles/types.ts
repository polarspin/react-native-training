import {
  loadArticles,
  loadArticlesSuccess,
  loadArticlesFail,
  loadMoreArticles,
  openArticle,
} from './actions';
import { Article, NewsError } from '../../services/newsService';

export interface ArticlesState {
  readonly loading: boolean;
  readonly loadingMore: boolean;
  readonly articles: Article[];
  readonly lastPageLoaded: number;
  readonly error?: NewsError;
}

export type LoadArticlesAction = ReturnType<typeof loadArticles>;
export type LoadMoreArticlesAction = ReturnType<typeof loadMoreArticles>;
export type LoadArticlesSuccessAction = ReturnType<typeof loadArticlesSuccess>;
export type LoadArticlesFailAction = ReturnType<typeof loadArticlesFail>;
export type OpenArticleAction = ReturnType<typeof openArticle>;

export type ArticlesAction = (
  LoadArticlesAction |
  LoadMoreArticlesAction |
  LoadArticlesSuccessAction |
  LoadArticlesFailAction |
  OpenArticleAction
);
