import { createStore, applyMiddleware, Middleware } from 'redux';
import { persistStore, persistCombineReducers, PersistConfig } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import { getLogicMiddleware } from './logic';
import { APP_TICK } from './app/actions';

import * as app from './app';
import * as nav from './nav';
import * as articles from './articles';
import * as notifications from './notifications';

const logics: any[] = [
  ...app.logic,
  ...nav.logic,
  ...articles.logic,
  ...notifications.logic,
];

const middlewares: Middleware[] = [
  getLogicMiddleware(logics, () => store),
];

const middleware = applyMiddleware(...middlewares);

const reducers: any = {
  app: app.reducer,
  nav: nav.reducer,
  articles: articles.reducer,
  notifications: notifications.reducer,
};

const persistConfig: PersistConfig = {
  key: 'app',
  storage,
  version: 1,
  debug: __DEV__,
  throttle: 1000,
  timeout: 50000,
  whitelist: [
  ],
};

const enhancedMiddleWare = composeWithDevTools({
  actionsBlacklist: [APP_TICK],
})(middleware);

export const store = createStore(
  persistCombineReducers(persistConfig, reducers),
  enhancedMiddleWare,
);

export const rehydrated = new Promise(resolve => {
  const persistor = persistStore(store, undefined, () => resolve(persistor));
});

export default store;
