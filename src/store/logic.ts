import Config from 'react-native-config';
import { createLogicMiddleware, LogicMiddleware } from 'redux-logic';
import { Store } from 'redux';
import { RootState } from './types';
import { Logger, logger } from '../logger';
import { NewsService } from '../services/NewsService';
import firebase, { Firebase } from 'react-native-firebase';

export interface LogicDependencies {
  logger: Logger;
  news: NewsService;
  firebase: Firebase;
}

export type LogicDeps<TAction extends any = undefined> = LogicDependencies & {
  action: TAction;
  getState(): RootState;
};

export const getLogicMiddleware = (logics: any[], getStore: () => Store): LogicMiddleware => {
  const logicDependencies: LogicDependencies = {
    logger,
    news: new NewsService(Config.NEWS_API_BASE_URL, Config.API_KEY),
    firebase,
  };
  return createLogicMiddleware(logics, logicDependencies);
};
