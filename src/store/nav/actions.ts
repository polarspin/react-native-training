import { createAction } from '../actionUtils';

export const NAVIGATE_TO = 'nav/NAVIGATE_TO';
export const NAVIGATE_TO_COMPLETED = 'nav/NAVIGATE_TO_COMPLETED';
export const NAVIGATE_TO_REJECTED = 'nav/NAVIGATE_TO_REJECTED';
export const NAVIGATE_BACK = 'nav/NAVIGATE_BACK';
export const OPEN_URL = 'nav/OPEN_URL';
export const OPEN_SIDEMENU = 'nav/OPEN_SIDEMENU';
export const SHOW_MODAL = 'nav/SHOW_MODAL';
export const DISMISS_MODAL = 'nav/DISMISS_MODAL';
export const SET_ACTIVE_SCREEN = 'nav/SET_ACTIVE_SCREEN';

export const navigateTo = (screen: string, args?: any) => createAction(NAVIGATE_TO, { screen, args });
export const navigateToCompleted = () => createAction(NAVIGATE_TO_COMPLETED);
export const navigateToRejected = () => createAction(NAVIGATE_TO_REJECTED);
export const navigateBack = () => createAction(NAVIGATE_BACK);
export const openURL = (url: string) => createAction(OPEN_URL, { url });
export const openSidemenu = () => createAction(OPEN_SIDEMENU);
export const showModal = (name: string, args?: any) => createAction(SHOW_MODAL, { name, args });
export const dismissModal = () => createAction(DISMISS_MODAL);
export const setActiveScreen = (name: string, id: string) => createAction(SET_ACTIVE_SCREEN, { name, id });
