import R from 'ramda';
import { Navigation } from 'react-native-navigation';
import { createLogic } from 'redux-logic';
import { LogicDeps } from '../logic';
import {
  NAVIGATE_TO,
  NAVIGATE_BACK,
  navigateToRejected,
  navigateToCompleted,
  OPEN_URL,
  openURL,
  DISMISS_MODAL,
  OPEN_SIDEMENU,
  SHOW_MODAL,
  showModal,
} from './actions';
import { getActiveComponent, getNavTarget } from './selectors';
import {
  NavigateToAction, OpenURLAction, ShowModalAction,
} from './types';

const navigateToLogic = createLogic({
  type: NAVIGATE_TO,
  validate: ({ getState, action }: LogicDeps<NavigateToAction>, allow, reject) => {
    const state = getState();
    const navTarget = getNavTarget(state);

    // To avoid double navigations we check if we are already navigating
    // to the same target and reject the duplicates.
    return R.equals(navTarget, action.payload)
      ? reject(navigateToRejected())
      : allow(action);
  },
  process: async ({ getState, action }: LogicDeps<NavigateToAction>, dispatch, done) => {
    const state = getState();
    const activeComponent = getActiveComponent(state);
    const { screen } = action.payload;

    await Navigation.push(activeComponent.id, {
      component: {
        name: screen,
        passProps: action.payload.args,
      },
    });
    dispatch(navigateToCompleted());
    done();
  },
});

const navigateBackLogic = createLogic({
  type: NAVIGATE_BACK,
  latest: true,
  process: async ({ logger, getState }: LogicDeps, dispatch, done) => {
    try {
      const state = getState();
      const activeComponent = getActiveComponent(state);

      await Navigation.pop(activeComponent.id);
    } catch (e) {
      logger.error('navigate back catch:', e);
    } finally {
      done();
    }
  },
});

const openURLLogic = createLogic({
  type: OPEN_URL,
  latest: true,
  process: async ({ logger, action }: LogicDeps<OpenURLAction>, dispatch, done) => {
    try {
      dispatch(showModal('WebViewScreen', action.payload));
    } catch (e) {
      logger.error('open URL catch:', e);
    } finally {
      done();
    }
  },
});

const showModalLogic = createLogic({
  type: SHOW_MODAL,
  latest: true,
  process: async ({ logger, action }: LogicDeps<ShowModalAction>, dispatch, done) => {
    try {
      await Navigation.showModal({
        stack: {
          children: [
            { component: { name: action.payload.name, passProps: action.payload.args }},
          ],
        },
      });
    } catch (e) {
      logger.error('open URL catch:', e);
    } finally {
      done();
    }
  },
});

const dismissModalLogic = createLogic({
  type: DISMISS_MODAL,
  process: async ({ logger, action }: LogicDeps<OpenURLAction>, dispatch, done) => {
    try {
      await Navigation.dismissAllModals();
    } catch (e) {
      logger.error('Dismiss modal fail', e);
    } finally {
      done();
    }
  },
});

const openSidemenuLogic = createLogic({
  type: OPEN_SIDEMENU,
  process: async ({ logger, action }: LogicDeps<OpenURLAction>, dispatch, done) => {
    try {
      await Navigation.mergeOptions('root', {
        sideMenu: {
          right: {
            visible: true,
          },
        },
      });
    } catch (e) {
      logger.error('Dismiss modal fail', e);
    } finally {
      done();
    }
  },
});

export default [
  navigateToLogic,
  navigateBackLogic,
  openURLLogic,
  showModalLogic,
  dismissModalLogic,
  openSidemenuLogic,
];
