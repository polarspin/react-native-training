import { NavState, NavAction } from './types';
import {
  NAVIGATE_TO,
  SET_ACTIVE_SCREEN,
  NAVIGATE_TO_COMPLETED,
  NAVIGATE_TO_REJECTED,
} from './actions';

const initialState: NavState = {
  navTarget: undefined,
  activeComponent: {
    id: '',
    name: '',
  },
};

export default (state: NavState = initialState, action: NavAction): NavState => {
  switch (action.type) {
    case NAVIGATE_TO:
      return {
        ...state,
        navTarget: action.payload,
      };
    case NAVIGATE_TO_REJECTED:
    case NAVIGATE_TO_COMPLETED:
      return {
        ...state,
        navTarget: undefined,
      };
    case SET_ACTIVE_SCREEN:
      return {
        ...state,
        activeComponent: action.payload,
      };
    default:
      return state;
  }
};
