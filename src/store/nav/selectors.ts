import { RootState } from '../types';

export const getActiveComponent = (state: RootState) => state.nav.activeComponent;
export const getNavTarget = (state: RootState) => state.nav.navTarget;
