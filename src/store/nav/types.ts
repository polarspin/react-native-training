import {
  navigateTo,
  navigateBack,
  setActiveScreen,
  navigateToCompleted,
  navigateToRejected,
  openURL,
  dismissModal,
  openSidemenu,
  showModal,
} from './actions';

export interface NavState {
  readonly navTarget?: {
    screen: string;
    args?: any;
  };
  readonly activeComponent: {
    id: string;
    name: string;
  };
}

export type NavigateToAction = ReturnType<typeof navigateTo>;
export type NavigateToCompletedAction = ReturnType<typeof navigateToCompleted>;
export type NavigateToRejectedAction = ReturnType<typeof navigateToRejected>;
export type NavigateBackAction = ReturnType<typeof navigateBack>;
export type OpenURLAction = ReturnType<typeof openURL>;
export type OpenSidemenuAction = ReturnType<typeof openSidemenu>;
export type ShowModalAction = ReturnType<typeof showModal>;
export type DismissModalAction = ReturnType<typeof dismissModal>;
export type SetActiveScreenAction = ReturnType<typeof setActiveScreen>;

export type NavAction = (
  NavigateToAction |
  NavigateToCompletedAction |
  NavigateToRejectedAction |
  NavigateBackAction |
  SetActiveScreenAction |
  OpenURLAction |
  OpenSidemenuAction |
  ShowModalAction |
  DismissModalAction
);
