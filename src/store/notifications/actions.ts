import { createAction } from '../actionUtils';

export const NOTIFICATION_RECEIVED = 'notifications/NOTIFICATION_RECEIVED';

export const notificationReceived = (data: any) => createAction(NOTIFICATION_RECEIVED, { data });
