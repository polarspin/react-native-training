import { createLogic } from 'redux-logic';
import { LogicDeps } from '../logic';
import {
  notificationReceived,
} from './actions';
import { APP_START } from '../app';

const startNotificationListenerOnAppStartLogic = createLogic({
  type: APP_START,
  process: async ({ firebase }: LogicDeps, dispatch, done) => {

    await firebase.messaging().requestPermission();

    const initialNotification = await firebase.notifications().getInitialNotification();
    if (initialNotification) {
      dispatch(notificationReceived(initialNotification));
    }

    firebase.notifications().onNotification(n => dispatch(notificationReceived(n)));
    firebase.notifications().onNotificationOpened(n => dispatch(notificationReceived(n)));
    done();
  },
});

export default [
  startNotificationListenerOnAppStartLogic,
];
