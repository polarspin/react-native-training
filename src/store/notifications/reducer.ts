import { NotificationsState, NotificationsAction } from './types';
import {
  NOTIFICATION_RECEIVED,
} from './actions';

const initialState: NotificationsState = {
  items: [],
};

export default (state: NotificationsState = initialState, action: NotificationsAction): NotificationsState => {
  switch (action.type) {
    case NOTIFICATION_RECEIVED:
      return {
        ...state,
        items: state.items.concat([action.payload.data]),
      };
    default:
      return state;
  }
};
