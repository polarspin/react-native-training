import { RootState } from '../types';

export const getNotifications = (state: RootState) => state.notifications.items;
