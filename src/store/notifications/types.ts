import {
  notificationReceived,
} from './actions';

export interface NotificationsState {
  readonly items: any[];
}

export type NotificationReceivedAction = ReturnType<typeof notificationReceived>;

export type NotificationsAction = (
  NotificationReceivedAction
);
