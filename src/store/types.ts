import { AppState } from './app/types';
import { NavState } from './nav/types';
import { ArticlesState } from './articles/types';
import { NotificationsState } from './notifications/types';

export interface RootState {
  readonly app: AppState;
  readonly nav: NavState;
  readonly articles: ArticlesState;
  readonly notifications: NotificationsState;
}
