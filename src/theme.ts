
const colors = {
  backgroundWhite: '#f0f0f0',
  backgroundBlack: '#222',
  backgroundGrey: '#e0e0e0',
  textBlack: '#222',
  textWhite: '#fff',
};

const fonts = {
  default: '',
};

const _theme = {
  colors,
  fonts,
};

export type Colors = typeof colors;
export type Fonts = typeof fonts;
export type Theme = typeof _theme;

export const theme: Theme = _theme;
